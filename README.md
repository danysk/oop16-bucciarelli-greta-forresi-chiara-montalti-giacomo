# Coinquify #

Coinquify è un'applicazione che gestisce la vita tra coinquilini in modo semplice e ottimale. 
Online ci sono svariate applicazioni che si occupano di organizzare vari aspetti della vita tra coinquilini anche se in realtà svolgono tutte funzionalità differenti.
Manca effettivamente un’applicazione unica per coordinare la vita dei coinquilini. 
Vivendo l’università da studenti fuorisede è nata quindi questa necessità.
Coinquify ha dunque l'obiettivo di aiutare i coinquilini di un appartamento, gestendo le spese, un’agenda in comune e altre informazioni, in modo da poter ottenere benefici concreti nella vita quotidiana.

### Il software esporrà le seguenti funzionalità: ###

* gestione dei turni delle varie mansioni casalinghe;
* un’agenda comune a tutti per gli appuntamenti;
* una rubrica condivisa;
* una bacheca comune con le regole delle casa e spazio per i post-it virtuali;
* lista della spesa condivisa;
* tenere conto dei soldi comuni (bollette, spesa comune, ecc..) e quindi anche dei debiti tra i vari coinquilini.