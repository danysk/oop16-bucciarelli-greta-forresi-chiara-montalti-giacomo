package it.unibo.coinquify.utils;

/**
 * Class with all public static final fields.
 */
public final class Constants {
    /**
     * max number of room mates.
     */
    public static final int MAX_ROOM_MATES = 10;
    /**
     * Chars of fiscal code.
     */
    public static final int FISCALCODE_LENGTH = 16;
    /**
     * Phone number max lenght.
     */
    public static final int PHONENUMBER_MAX_LENGTH = 15;
    /**
     * Phone number min lenght.
     */
    public static final int PHONENUMBER_MIN_LENGTH = 10;
    /**
     * Component's per rows in display.
     */
    public static final int COMPONENTS_FOR_ROW = 2;
    /**
     * Empty number.
     */
    public static final String EMPTY_NUMBER = "__________";

    private Constants() {
    }
}
