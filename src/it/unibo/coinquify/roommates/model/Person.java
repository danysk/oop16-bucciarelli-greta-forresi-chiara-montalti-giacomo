package it.unibo.coinquify.roommates.model;

/**
 * Person interface.
 */
public interface Person {
    /**
     * 
     * @return the name
     */
    String getName();

    /**
     * 
     * @return the surname
     */
    String getSurname();

    /**
     * 
     * @return the fiscal code
     */
    String getFiscalCode();

    /**
     * 
     * @return the RoomMate's phone number
     */
    String getPhoneNumber();
}
